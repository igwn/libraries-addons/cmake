if(NOT EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/Dockerfile) 
    message(WARNING "Dockerfile not found in this project")
else()

    # Look for git repository path
    if(NOT DEFINED DOCKER_IMAGE)
    
        find_package(Git)
        execute_process(
            COMMAND ${GIT_EXECUTABLE} ls-remote --get-url origin
            WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
            OUTPUT_VARIABLE GIT_REMOTE_URL
            OUTPUT_STRIP_TRAILING_WHITESPACE
        )

        if(GIT_REMOTE_URL)

            if(DOCKER_IMAGE)
                message(STATUS "No docker image name provided. Will use git remote url path.")
            endif()
            string(REGEX REPLACE "(.*://|[^@]+@|[^/]+:)[^/]+/(.*)(\\.git)$" "\\2" DOCKER_IMAGE ${GIT_REMOTE_URL})

        elseif(NOT DEFINED DOCKER_IMAGE)
            message(WARNING "Could not retrieve docker image information using git origin. Please provide a name using -DDOCKER_IMAGE=[..]")
        endif()
        
    endif()

    if(DEFINED DOCKER_IMAGE)

        message(STATUS "Docker image name used `${DOCKER_IMAGE}`")
        if("${DOCKER_REGISTRY}" STREQUAL "")
            message(STATUS "No docker registry provided. By default, it will be published on Docker Hub. Please provide -DDOCKER_REGISTRY=[..]")
        elseif(DOCKER_IMAGE)
            set(DOCKER_IMAGE "${DOCKER_REGISTRY}/${DOCKER_IMAGE}")
            message(STATUS "Docker container will be published to ${DOCKER_IMAGE}")
        endif()

        # Add custom target to build the Docker image
        add_custom_target("image"
            COMMAND docker build -t ${DOCKER_IMAGE} .
            WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
            COMMENT "Building Docker image..."
        )

        # Add custom target to publish the Docker image
        add_custom_target("image-publish"
            COMMAND docker login ${DOCKER_REGISTRY}
            COMMAND docker push ${DOCKER_IMAGE}
            WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
            COMMENT "Publishing Docker image..."
        )
    endif()

endif()
