# Locate Google Test
find_package(GTest REQUIRED)
include_directories(${GTEST_INCLUDE_DIRS})

# Enable testing
enable_testing()

# Include CTest module for managing tests
include(CTest)

# Add a macro for adding tests
macro(add_gtest TEST_NAME TEST_SOURCES)
    
    add_executable(${TEST_NAME} ${TEST_SOURCES})
    target_link_libraries(${TEST_NAME} PRIVATE ${GTEST_BOTH_LIBRARIES} pthread)
        
    add_test(NAME ${TEST_NAME} COMMAND ${TEST_NAME})

endmacro()
