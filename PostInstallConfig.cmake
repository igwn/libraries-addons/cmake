include(${CMAKE_CURRENT_LIST_DIR}/MessageColor.cmake)
if("${CMAKE_SOURCE_DIR}" STREQUAL "${CMAKE_CURRENT_SOURCE_DIR}")

    if(CMAKE_INSTALL_PREFIX MATCHES "^/usr.*" OR
       CMAKE_INSTALL_PREFIX MATCHES "^/opt.*" OR
       CMAKE_INSTALL_PREFIX STREQUAL "/usr/local")
        message(STATUS "${COLOR_GREEN}Congratulations!${COLOR_RESET} This library has been installed into the system directory `${CMAKE_INSTALL_PREFIX}`")
    else()
        message("${COLOR_GREEN}-- Congratulations!${COLOR_RESET} This library has been installed into a user-defined directory `${CMAKE_INSTALL_PREFIX}`")
        message("${COLOR_YELLOW}-- Don't forget to source the library configuration in `${CMAKE_INSTALL_PREFIX}`, or add it to your profile:${COLOR_RESET}")

        # Get the SHELL environment variable
        get_filename_component(SHELLPATH "$ENV{SHELL}" NAME)

        foreach(FILEPATH ${CMAKE_ABSOLUTE_DESTINATION_FILES})

            if(NOT EXISTS ${FILEPATH})
                continue()
            endif()

            if(SHELLPATH MATCHES "csh$" AND FILEPATH MATCHES "\\.csh$")
                message("  ${COLOR_YELLOW} \$ source${COLOR_RESET} ${FILEPATH}")
            elseif(FILEPATH MATCHES "\\.sh$")
                message("  ${COLOR_YELLOW} \$ source${COLOR_RESET} ${FILEPATH}")
            elseif(FILEPATH MATCHES "C$" OR FILEPATH MATCHES "rootlogon$")
                message("  ${COLOR_YELLOW} \$ root${COLOR_RESET} ${FILEPATH}")
            endif()
        endforeach()

    endif()

endif()
