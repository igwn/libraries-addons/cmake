# Extra CMake Modules (ECM)

This repository contains the Extra CMake Modules (ECM), a collection of additional modules for CMake. These modules extend the functionality provided by CMake, including those used by find_package() to locate common software, modules usable directly in CMakeLists.txt files for performing routine tasks, and toolchain files that users must specify on the command line.

## ECM Overview

ECM also offers common build settings utilized in software developed by the KDE community. While its primary focus is reducing duplication in CMake scripts across KDE software, it is designed to be beneficial for any project employing the CMake build system.

For detailed information and usage instructions, please refer to the official ECM documentation.

## Usage
To integrate ECM into your CMake-based project, follow these steps:

Clone this repository at the root of your project. 

```
mkdir -p cmake
git clone https://<your-url>/<path>/cmake.git ./cmake/Modules
```

Add ECM to your project's CMakeLists.txt file by adding the repository into the CMAKE_MODULE_PATH. You can now use the `include` instruction to load it. Some modules defines variables, macros or function and are meant to simplify your use of cmake with minimal dependencies.

```
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "/path/to/cmake/Modules/")
include(<YourModule>)
```

e.g. YourModule = "DisableInSourceBuild"

# Main modules

| Module Name                   | Description                                                                   |
|-------------------------------|-------------------------------------------------------------------------------|
| CheckClangCompiler.cmake      | This module provides functionality to check for the Clang compiler.| 
| DisableInSourceBuild.cmake    | This module helps in disabling in-source builds, promoting out-of-source builds.| 
| DockerContainer.cmake         | Module related to Docker container configurations. | 
| DoxygenProjectVariables.cmake| This module sets up variables for Doxygen documentation generation.| 
| FindPackageStandard.cmake     | Module for finding standard packages using CMake's `find_package_standard()`. | 
| GitBranchWarning.cmake        | Provides warnings related to the Git branch during the CMake configuration process.| 
| LoadStandardCompilerC.cmake  | Module to load standard C compiler configurations.                      
| LoadStandardCompilerCXX.cmake| Module to load standard C++ compiler configurations.|
| LoadStandardCompilerF.cmake  | Module to load standard Fortran compiler configurations. |
| LoadTests.cmake               | Module to load tests for the project. |
| MessageColor.cmake            | Module for coloring messages printed during the CMake configuration process.  |
| ParentCMakeOnly.cmake         | Module restricting usage to parent CMake projects only.                       | 
| PostCMakeConfig.cmake         | Module for performing actions after CMake configuration.                      | 
| PostInstallConfig.cmake       | Module for performing actions after installation.                             | 
| ProjectArchitecture.cmake     | Module for setting up project architecture configurations.                    |
