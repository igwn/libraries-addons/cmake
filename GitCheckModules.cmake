# GitCheckModules.cmake

# Function to check if a submodule exists and is initialized
function(check_submodule GITPATH)
    
    if(NOT EXISTS ${GITPATH})
        message(FATAL_ERROR "Git submodule '${GITPATH}' does not exist. Please initialize submodules using `git submodule update --init --recursive`.")
    endif()

    file(READ "${GITPATH}/.git" GITDIR)
    string(REGEX MATCH "^[[:space:]]*gitdir:[[:space:]]*(.+)" _ "${GITDIR}")

    if(NOT DEFINED CMAKE_MATCH_1)
        message(FATAL_ERROR "Git submodule '${GITPATH}' is not initialized. Please initialize submodules using `git submodule update --init --recursive`.")
    endif()
endfunction()

# Read .gitmodules file to find submodule paths
file(READ "${CMAKE_SOURCE_DIR}/.gitmodules" GITMODULES_CONTENT)

# Regex to find paths in .gitmodules
string(REGEX MATCHALL "path *= *[^\n]+" SUBMODULE_PATHS "${GITMODULES_CONTENT}")
# Extract the paths and check each submodule
foreach(SUBMODULE_PATH ${SUBMODULE_PATHS})
    string(REGEX REPLACE "path *= *" "" SUBMODULE_PATH "${SUBMODULE_PATH}")
    string(STRIP "${SUBMODULE_PATH}" SUBMODULE_PATH)  # Remove any leading or trailing whitespace
    check_submodule(${CMAKE_SOURCE_DIR}/${SUBMODULE_PATH})
endforeach()

message(STATUS "All submodules are initialized.")