
# Check supported C standards
include(CheckCCompilerFlag)

# Check for different C standards
check_c_compiler_flag("-std=c90" COMPILER_SUPPORTS_C90)
check_c_compiler_flag("-std=c99" COMPILER_SUPPORTS_C99)
check_c_compiler_flag("-std=c11" COMPILER_SUPPORTS_C11)
check_c_compiler_flag("-std=c17" COMPILER_SUPPORTS_C17)
check_c_compiler_flag("-std=c23" COMPILER_SUPPORTS_C23)

if(COMPILER_SUPPORTS_C23)
    set(SUPPORTED_C_STANDARDS 23 17 11 99 90)
elseif(COMPILER_SUPPORTS_C17)
    set(SUPPORTED_C_STANDARDS 17 11 99 90)
elseif(COMPILER_SUPPORTS_C11)
    set(SUPPORTED_C_STANDARDS 11 99 90)
elseif(COMPILER_SUPPORTS_C99)
    set(SUPPORTED_C_STANDARDS 99 90)
elseif(COMPILER_SUPPORTS_C90)
    set(SUPPORTED_C_STANDARDS 90)
else()
    message(FATAL_ERROR "No supported C standard found.")
endif()

# Allow manual override of C standard
if(NOT DEFINED CMAKE_C_STANDARD)
    # Use the highest supported C standard as default
    set(HIGHEST_SUPPORTED_C_STANDARD ${SUPPORTED_C_STANDARDS})
    list(GET HIGHEST_SUPPORTED_C_STANDARD 0 CMAKE_C_STANDARD)
endif()

# Print the selected C standard
if(NOT CMAKE_C_HASH STREQUAL "${CMAKE_C_COMPILER}${CMAKE_C_STANDARD}")
    
    # Update the cached hash value
    set(CMAKE_C_HASH "${CMAKE_C_COMPILER}${CMAKE_C_STANDARD}" CACHE INTERNAL "Hash of CMAKE_C_COMPILER and CMAKE_C_STANDARD")

    # Only print the message when CMAKE_C_COMPILER or CMAKE_C_STANDARD has changed
    if(CMAKE_C_COMPILER)
        message(STATUS "Found C${CMAKE_C_STANDARD} standard compiler: ${CMAKE_C_COMPILER}")
    else()
        message(FATAL_ERROR "No C standard compiler found.")
    endif()
endif()