# Define ANSI escape codes for colors
string(ASCII 27 ESC)
set(COLOR_RESET "${ESC}[0m")
set(COLOR_BLACK "${ESC}[30m")
set(COLOR_RED "${ESC}[31m")
set(COLOR_GREEN "${ESC}[32m")
set(COLOR_YELLOW "${ESC}[33m")
set(COLOR_BLUE "${ESC}[34m")
set(COLOR_MAGENTA "${ESC}[35m")
set(COLOR_CYAN "${ESC}[36m")
set(COLOR_WHITE "${ESC}[37m")
set(COLOR_BOLD_BLACK "${ESC}[1;30m")
set(COLOR_BOLD_RED "${ESC}[1;31m")
set(COLOR_BOLD_GREEN "${ESC}[1;32m")
set(COLOR_BOLD_YELLOW "${ESC}[1;33m")
set(COLOR_BOLD_BLUE "${ESC}[1;34m")
set(COLOR_BOLD_MAGENTA "${ESC}[1;35m")
set(COLOR_BOLD_CYAN "${ESC}[1;36m")
set(COLOR_BOLD_WHITE "${ESC}[1;37m")

# Print color
macro ( print_color NAME )
    message_color ( COLOR ${NAME} "     ${NAME}" )
endmacro ()

# Message color function
function  ( text )
    cmake_parse_arguments ( PARSE_ARGV 0 "_TEXT" "BOLD" "COLOR" "" )

    set ( _TEXT_OPTIONS -E cmake_echo_color --no-newline )

    if ( _TEXT_COLOR )
        string ( TOLOWER "${_TEXT_COLOR}" _TEXT_COLOR_LOWER )
        if ( NOT ${_TEXT_COLOR_LOWER} MATCHES "^normal|black|red|green|yellow|blue|magenta|cyan|white" )
            message( FATAL_ERROR "Color ${_TEXT_COLOR} is not support. (Only these colours are supported: \"NORMAL BLACK RED GREEN YELLOW BLUE MAGENTA CYAN WHITE\")" )
        else ()
            list ( APPEND _TEXT_OPTIONS --${_TEXT_COLOR_LOWER} )
        endif ()
    endif ()

    if ( _TEXT_BOLD )
        list ( APPEND _TEXT_OPTIONS --bold )
    endif ()

    execute_process ( COMMAND ${CMAKE_COMMAND} -E env CLICOLOR_FORCE=1 ${CMAKE_COMMAND} ${_TEXT_OPTIONS} ${_TEXT_UNPARSED_ARGUMENTS}
                      OUTPUT_VARIABLE _TEXT_RESULT
                      ECHO_ERROR_VARIABLE
                      )

    set ( TEXT_RESULT ${_TEXT_RESULT} PARENT_SCOPE )
endfunction ()
unset ( print_color )

function ( message_color )
    text ( ${ARGN} )
    message ( ${TEXT_RESULT} )
endfunction ()